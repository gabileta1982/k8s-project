# k8s-project

Este repositorio contiene los archivos necesarios para poder crear un cluster de kubernetes, levantar una aplicación y exponerla mediante un proxy-reverso usando nginx.

## Descarga e Instalación de Minikube

[Dcoumentación Minikube](https://minikube.sigs.k8s.io/docs/start/)

En mi caso baje el paquete para poder instalarlo en Ubuuntu.

![minikube_install](docs/images/minikube_install.png)

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```

## Creación del cluster

Una vez instalado, podemos proceder a crear el cluster:

minikube start --nodes=<Cantidad_Nodos> -p <Nombre_Cluster>

Por ejemplo, podemos crear un cluster con dos nodos:

```
minikube start --nodes=2 -p k8s-tp-nodes
```

## Listar nodos del cluster
```
kubectl get nodes
```

## Creación del Persistent Volume

Para poder crear el volumen que luego va a ser montado a través de un claim contra el deploy o pod que lo necesitemos debemos correr el siguiente comando

*k8s-pv.yaml*
```
apiVersion: v1
kind: PersistentVolume
metadata:
 name: k8s-tp-pv
 labels:
  type: local
spec:
 storageClassName: manual
 capacity:
  storage: 1Gi
 accessModes:
   - ReadWriteOnce
 hostPath:
   path: "/mnt/logs/"

```
```
kubectl create -f k8s-pv.yaml
```

### Listar Persistent Volumes
```
kubectl get pv
```

## Creacion del PVClaim

*k8s-pvc.yaml*
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
 name: k8s-tp-pvc
spec:
 storageClassName: manual
 accessModes:
   - ReadWriteOnce
 resources:
   requests:
     storage: 1Gi
```
```
kubectl create -f k8s-pvc.yaml
```

### Listar Persistent Volume Claims
```
kubectl get pvc
```

## Creación del deployment
En este caso estamos desplegando dos aplicaciones. Dentro del deployment se encuentra la configuración para los replica set y por defecto la creación del pod con los parámetros necesarios para el container.

*k8s-deploy.yaml*
```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: k8s-tp-deploy
  name: k8s-tp-deploy
  namespace: default
spec:
  replicas: 3
  selector:
    matchLabels:
      app: k8s-tp-deploy
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: k8s-tp-deploy
    spec:
      containers:
      - image: in28min/hello-world-rest-api:0.0.1.RELEASE
        imagePullPolicy: IfNotPresent
        name: hello-world-rest-api
        volumeMounts:
          - mountPath: "/tmp/logs"
            name: logs
        ports:
          - containerPort: 8080
      restartPolicy: Always
      volumes:
      - name: logs
        persistentVolumeClaim:
          claimName: k8s-tp-pvc
      terminationGracePeriodSeconds: 30
```
```
kubectl create -f k8s-deploy.yaml
```

*k8s-deploy-2.yaml*
```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: k8s-tp-deploy-2
  name: k8s-tp-deploy-2
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: k8s-tp-deploy-2
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: k8s-tp-deploy-2
    spec:
      containers:
      - image: gabileta1982/k8s-project:v1
        imagePullPolicy: IfNotPresent
        name: principito
        ports:
          - containerPort: 80
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
```
```
kubectl create -f k8s-deploy-2.yaml
```

Como se puede observar en la configuración de ambos, para el primer deploy estamos montando el volumen creado previamente, no así en el segundo deploy.

![describe_pods_mount](docs/images/describe_pods_mount.png)

### Listar deployment, replicaset y pod
```
kubectl get deploy
```
![list_deploy](docs/images/k8s_list_deploy.png)
```
kubectl get rs
```
![list_replica_set](docs/images/k8s_list_rs.png)
```
kubectl get pod
```
![list_pod](docs/images/k8s_list_pod.png)

## Creación de los servicio
Tenemos dos aplicaciones en deployments separados, para poder darle exposición por fuera del cluster debemos crear un servicio. En este caso optamos por usar el tipo nodePort, donde se puede acceder al mismo usando la IP externa del cluster junto con el puerto que configuremos para poder acceder al servicio y su vez al deployment / aplicación que tenga asociado.

Usando los labels podemos mapear el servicio con el deploy.

*k8s-service.yaml*
```
apiVersion: v1
kind: Service
metadata:
  labels:
    app: k8s-tp-deploy
  name: k8s-tp-deploy
  namespace: default
spec:
  ports:
  - nodePort: 30741
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: k8s-tp-deploy
  sessionAffinity: None
  type: NodePort
```
```
kubectl create -f k8s-service.yaml
```

*k8s-service-2.yaml*
```
apiVersion: v1
kind: Service
metadata:
  labels:
    app: k8s-tp-deploy-2
  name: k8s-tp-deploy-2
  namespace: default
spec:
  ports:
  - nodePort: 30742
    port: 8090
    protocol: TCP
    targetPort: 80
  selector:
    app: k8s-tp-deploy-2
  sessionAffinity: None
  type: NodePort

```
```
kubectl create -f k8s-service-2.yaml
```

Ambos tienen configurado puertos distintos:

*containerPort* - El puerto donde el docker/container expone el servicio.

*port* - El puerto que va a tener configurado dentro del cluster de kubernetes.

*nodePort* - El puerto expuesto por fuera del cluster.


## Configuración del proxy-reverso

Ya contamos con nuestros servicios expuestos por fuera del cluster de kubernetes.
Ambos pueden ser accedidos por la URL externa del cluster y el puerto expuesto por "nodePort".

Con el siguiente comando podemos ver que IP y puertos quedaron expuestos para consumir el servicio.

minikube service list -p <Nombre_Cluster>

```
minikube service list -p k8s-tp-nodes
```

Como se puede observar en la imagen, tenemos los dos servicios expuestos con sus puertos internos (8080 / 8090) y externos (30741 / 30742) al cluster.

![minikube_svc_list](docs/images/minikube_cluster_service_list.png)


Ahora vamos a configurar un nginx como proxy reverso, para poder exponer el servicio por http ó https (en caso que se requiera) en vez de un puerto customizado y a su vez poder balancear entre los distintos nodos del cluster.

El mismo tendrá una configuración básica

### Creamos el dockerfile

*dockerfile*
```
FROM nginx
COPY nginx.conf /etc/nginx/nginx.conf
```

### Creamos el archivo de configuración

*nginx.conf*

```
events {
   worker_connections  10240;
 }

 http {
   upstream myservers {
     server k8s-tp-nodes:30741;
     server k8s-tp-nodes-m02:30741;
   }
   upstream myservers2 {
     server k8s-tp-nodes:30742;
     server k8s-tp-nodes-m02:30742;
   }

   server {
       listen 80;
       server_name _;
       location /java {
           proxy_pass http://myservers/;
       }
       location /principito {
           proxy_pass http://myservers2/;
       }
   }
 }
```

Se crea el listener en el :80 (http) y se crean dos balanceadores, uno para cada servicio.
Vamos a mapear el /java con el primer balanceador y el /principito con el segundo.

Dentro de cada balancer tiene el puerto expuesto por fuera del cluster, y se indica cada uno de los nodos. También podría indicarse la IP del cluster y el puerto específico tal como nos mostraba la imagen previa.


### Buildeamos la imagen
```
docker build -t custom-nginx .
```

### Con este comando vamos a generar el contenedor con la imagen de nginx, exponerlo en el puerto 80 y decirle que comparta la red con el nodo master del cluster de kubernetes

```
docker run --network=k8s-tp-nodes --name nginx -d -p 80:80 custom-nginx
```

### Verificamos que esté corriendo
![docker_list](docs/images/docker_list.png)


En este caso estamos levantando el nginx en nuestro localhost, sin indicarle ninguna URL en particular, por lo cual para poder acceder a cada servicio deberíamos usar la URL

http://localhost/java/hello-world

http://localhost/principito


### Imagenes usadas
https://hub.docker.com/r/in28min/hello-world-rest-api/tags

https://hub.docker.com/r/gabileta1982/k8s-project/tags (proyecto previo)



## Borrado
Para poder borrar podemos ir haciendo el rm / delete de cada componente, ya sea docker y kubernetes como así el cluster creado por minikube.

### Eliminar nginx
```
docker kill $(docker ps -a | grep custom-nginx | awk '{print $1}')
docker rm $(docker ps -a | grep custom-nginx | awk '{print $1}')
```

### Eliminar servicios
```
kubectl delete svc k8s-tp-deploy
kubectl delete svc k8s-tp-deploy-2
```

### Eliminar deployments
```
kubectl delete deploy k8s-tp-deploy
kubectl delete deploy k8s-tp-deploy-2
```

### Eliminar PVClaim
```
kubectl delete pvc k8s-tp-pvc
```

### Eliminar PV
```
kubectl delete pv k8s-tp-pv
```

### Eliminar cluster Minikube
```
minikube delete --all
```